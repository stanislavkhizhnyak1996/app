insert into platforms(number, name) values(0, 'web');
insert into platforms(number, name) values(1, 'android');
insert into platforms(number, name) values(2, 'ios');

insert into status(number, name) values(0, 'planned');
insert into status(number, name) values(1, 'active');
insert into status(number, name) values(2, 'paused');
insert into status(number, name) values(2, 'finished');
