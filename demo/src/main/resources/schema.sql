Create table platforms (id INTEGER NOT null primary key auto_increment,
                        number integer,
                        name varchar(255));

Create table status (id INTEGER NOT null primary key auto_increment,
                     number integer,
                     name varchar(255));

CREATE TABLE campaigns (id INTEGER NOT null primary key auto_increment,
                        name varchar(255),
                        status_id INTEGER,
                        start_date date,
                        end_date date,
                        foreign key (status_id) references status(id));

create table ads (id INTEGER NOT null primary key auto_increment,
                  name varchar(255),
                  status_id integer,
                  patforms integer,
                  assert_url varchar(255),
                  campaign_id integer,
                  foreign key (status_id) references status(id),
                  foreign key (campaign_id) references campaigns(id));

Create table ads_platforms (ad_id integer, platform_id integer,
                            foreign key (ad_id) references ads(id),
                            foreign key (platform_id) references platforms(id));
