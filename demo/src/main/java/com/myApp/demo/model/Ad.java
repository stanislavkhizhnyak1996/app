package com.myApp.demo.model;

import lombok.Data;

import java.util.List;

@Data
public class Ad {
    private int id;
    private String name;
    private Status status;
    private List<Platform> platform;
    private String assetUrl;
    private int campaignsId;
}
