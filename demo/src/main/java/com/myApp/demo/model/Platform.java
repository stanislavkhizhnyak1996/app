package com.myApp.demo.model;

import lombok.Data;

@Data
public class Platform {
    private int id;
    private int number;
    private String platform;
}
