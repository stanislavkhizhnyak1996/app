package com.myApp.demo.model;

import lombok.Data;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Data
public class Campaign {
    private int id;
    private String name;
    private Status status;
    private LocalDate startDate;
    private LocalDate endDate;
    private List<Ad> ads;
}
