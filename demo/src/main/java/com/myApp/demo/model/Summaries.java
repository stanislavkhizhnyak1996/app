package com.myApp.demo.model;

import lombok.Data;

@Data
public class Summaries {
    private long id;
    private String campaignsName;
    private Status status;
    private int countAds;
}
