package com.myApp.demo.model;

import lombok.Data;

@Data
public class Status {
    private int id;
    private int number;
    private String state;
}
