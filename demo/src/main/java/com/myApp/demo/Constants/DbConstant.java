package com.myApp.demo.Constants;

public class DbConstant {
    public static final int PLANED_STATUS_ID = 1;
    public static final int ACTIVE_STATUS_ID = 2;
    public static final int PAUSED_STATUS_ID = 3;
    public static final int FINISHED_STATUS_ID = 4;
}
