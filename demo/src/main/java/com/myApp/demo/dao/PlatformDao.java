package com.myApp.demo.dao;

import com.myApp.demo.dto.PlatformDto;
import com.myApp.demo.model.Platform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PlatformDao {
    private DataSource dataSource;

    @Autowired
    public PlatformDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Platform> getPlatformsForAdByAdId(int id) {

        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement()
        ) {
            String getIdsPlatformIncludesInAd = "SELECT * FROM ads_platforms WHERE ad_id = " + id;
            ResultSet resultSetIds = statement.executeQuery(getIdsPlatformIncludesInAd);
            List<Long> platformIds = new ArrayList<>();
            while (resultSetIds.next()) {
                long re = resultSetIds.getLong("platform_id");
                platformIds.add(re);
            }

            List<Platform> platforms = new ArrayList<>();
            for (Long platformId : platformIds) {
                String getPlatformSql = "SELECT * FROM platforms WHERE id = " + platformId.toString() + ";";
                ResultSet resultSet = statement.executeQuery(getPlatformSql);
                Platform platform = new Platform();
                while (resultSet.next()) {
                    platform.setId(resultSet.getInt("id"));
                    platform.setNumber(resultSet.getInt("number"));
                    platform.setPlatform(resultSet.getString("name"));
                    platforms.add(platform);
                }
            }
            return platforms;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public void addPlatformToAdByAdId(int adId, int platformId) {
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement()) {
            String sql = "INSERT INTO ads_platforms (ad_id, platform_id) VALUES(" + adId + "," + platformId + ");";
            statement.execute(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void updatePlatformForAdByAdId(int adId, List<PlatformDto> platforms) {
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement()) {
            deleteAdPlatformsByAdId(adId);
            if(!platforms.isEmpty()) {
                for (PlatformDto platform : platforms) {
                    addPlatformToAdByAdId(adId, platform.getId());
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void deleteAdPlatformsByAdId(long adId){
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement()) {
            String sql = "DELETE FROM ads_platforms WHERE ad_id = (" + adId + ");";
            statement.execute(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
