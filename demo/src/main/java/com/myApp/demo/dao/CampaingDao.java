package com.myApp.demo.dao;

import com.myApp.demo.dto.AdDto;
import com.myApp.demo.dto.CampaignDto;
import com.myApp.demo.model.Campaign;
import com.myApp.demo.model.Status;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.myApp.demo.Constants.DbConstant.PLANED_STATUS_ID;

@Repository
public class CampaingDao {
    private DataSource dataSource;
    private AdDao adDao;
    private StatusDao statusDao;
    private ModelMapper modelMapper;

    @Autowired
    public CampaingDao(DataSource dataSource, AdDao adDao, StatusDao statusDao, ModelMapper modelMapper) {
        this.dataSource = dataSource;
        this.adDao = adDao;
        this.statusDao = statusDao;
        this.modelMapper = modelMapper;
    }

    public CampaignDto getById(int id) {
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement()) {
            String getCampaignsById = "SELECT * FROM campaigns WHERE id = " + id + ";";
            ResultSet campaign = statement.executeQuery(getCampaignsById);
            Campaign campaignFromDb = new Campaign();
            while (campaign.next()) {
                campaignFromDb.setId(campaign.getInt("id"));
                campaignFromDb.setStartDate(campaign.getDate("start_date").toLocalDate());
                campaignFromDb.setEndDate(campaign.getDate("end_date").toLocalDate());
                campaignFromDb.setName(campaign.getString("name"));

                String selectStatus = "SELECT * FROM status WHERE id = " + campaign.getInt("status_id");
                ResultSet resultSet = con.createStatement().executeQuery(selectStatus);
                Status status = new Status();
                while (resultSet.next()) {
                    status.setId(resultSet.getInt("id"));
                    status.setNumber(resultSet.getInt("number"));
                    status.setState(resultSet.getString("name"));
                }
                campaignFromDb.setStatus(status);
            }
            CampaignDto campaignDto = modelMapper.map(campaignFromDb, CampaignDto.class);
            campaignDto.setAds(adDao.getAdsByCampaignsId(id));
            return campaignDto;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public CampaignDto add(CampaignDto campaign) {
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement()) {
            String sqlCreate = "INSERT INTO campaigns (name, status_id, start_date, end_date) VALUES ("
                + "'" + campaign.getName() + "', "
                + PLANED_STATUS_ID + ", "
                + "'" + java.sql.Date.valueOf(campaign.getStartDate()) + "'" + ", "
                + "'" + java.sql.Date.valueOf(campaign.getStartDate()) + "'" + ")";
            statement.execute(sqlCreate, Statement.RETURN_GENERATED_KEYS);
            int newCampaignsId = 0;
            ResultSet generatedKeys = statement.getGeneratedKeys();
            while (generatedKeys.next()) {
                newCampaignsId = generatedKeys.getInt(1);
                campaign.setId(newCampaignsId);
            }


            List<AdDto> newAdsFromDb = new ArrayList<>();
            for (AdDto ad : campaign.getAds()) {
                ad.setCampaignsId(newCampaignsId);
                AdDto adFromDb = adDao.add(ad);
                newAdsFromDb.add(adFromDb);
            }


            String selectStatus = "SELECT * FROM status WHERE id = " + PLANED_STATUS_ID;
            ResultSet resultSet = statement.executeQuery(selectStatus);
            Status status = new Status();
            while (resultSet.next()) {
                status.setId(resultSet.getInt("id"));
                status.setNumber(resultSet.getInt("number"));
                status.setState(resultSet.getString("name"));
            }

            campaign.setStatus(status);
            campaign.setAds(newAdsFromDb);
            return modelMapper.map(campaign, CampaignDto.class);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public CampaignDto update(CampaignDto campaign) {
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement()) {
            for (AdDto ad : campaign.getAds()) {
                adDao.update(ad);
            }
            String updateSql = "UPDATE campaigns SET name = '"
                + campaign.getName()
                + "', status_id = " + campaign.getStatus().getId()
                + ", start_date = '" + campaign.getStartDate()
                + "', end_date = '" + campaign.getEndDate()
                + "' WHERE id = " + campaign.getId() + ";";
            statement.executeUpdate(updateSql, Statement.RETURN_GENERATED_KEYS);
            int updatedCampaignId = 0;
            ResultSet generatedKeys = statement.getGeneratedKeys();
            while (generatedKeys.next()) {
                updatedCampaignId = generatedKeys.getInt(1);
            }
            return getById(updatedCampaignId);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public boolean delete(int id) {
        adDao.deleteByCampaignId(id);
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement()) {
            String deleteSql = "DELETE FROM campaigns WHERE id = " + id + ";";
            statement.execute(deleteSql);
            statement.close();
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }
}
