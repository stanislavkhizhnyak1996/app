package com.myApp.demo.dao;

import com.myApp.demo.dto.CampaignDto;
import com.myApp.demo.dto.Filter;
import com.myApp.demo.dto.SummariesDto;
import com.myApp.demo.model.Campaign;
import com.myApp.demo.model.Status;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SummariesDao {
    private DataSource dataSource;
    private AdDao adDao;
    private StatusDao statusDao;
    private ModelMapper modelMapper;

    public SummariesDao(DataSource dataSource, AdDao adDao, StatusDao statusDao, ModelMapper modelMapper) {
        this.dataSource = dataSource;
        this.adDao = adDao;
        this.statusDao = statusDao;
        this.modelMapper = modelMapper;
    }

    public List<SummariesDto> getAll() {
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement()) {
            String getCampaignsByParam = "SELECT * FROM campaigns;";
            ResultSet campaign = statement.executeQuery(getCampaignsByParam);
            List<CampaignDto> campaignsFromDb = new ArrayList<>();
            while (campaign.next()) {
                Campaign campaignFromDb = new Campaign();
                campaignFromDb.setId(campaign.getInt("id"));
                campaignFromDb.setStartDate(campaign.getDate("start_date").toLocalDate());
                campaignFromDb.setEndDate(campaign.getDate("end_date").toLocalDate());
                campaignFromDb.setName(campaign.getString("name"));


                String selectStatus = "SELECT * FROM status WHERE id = " + campaign.getInt("status_id");
                ResultSet resultSet = con.createStatement().executeQuery(selectStatus);
                Status status = new Status();
                while (resultSet.next()) {
                    status.setId(resultSet.getInt("id"));
                    status.setNumber(resultSet.getInt("number"));
                    status.setState(resultSet.getString("name"));
                }

                campaignFromDb.setStatus(status);
                campaignsFromDb.add(modelMapper.map(campaignFromDb, CampaignDto.class));
            }

            List<SummariesDto> summariesDtos = new ArrayList<>();
            campaignsFromDb.forEach(campaignDto -> {
                SummariesDto summariesDto = new SummariesDto();
                summariesDto.setId(campaignDto.getId());
                summariesDto.setStatus(campaignDto.getStatus());
                summariesDto.setCampaignsName(campaignDto.getName());
                summariesDto.setCountAds(adDao.getCountAdsByCampaignsId(campaignDto.getId()));
                summariesDtos.add(summariesDto);
            });
            campaign.close();
            return summariesDtos;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public List<SummariesDto> getByParam(Filter filter) {
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement()) {
            String getCampaignsByParam = "SELECT * FROM campaigns WHERE " + filter.getName() + " = " + filter.getParam() + ";";
            ResultSet campaign = statement.executeQuery(getCampaignsByParam);
            List<CampaignDto> campaignsFromDb = new ArrayList<>();
            while (campaign.next()) {
                Campaign campaignFromDb = new Campaign();
                campaignFromDb.setId(campaign.getInt("id"));
                campaignFromDb.setStartDate(campaign.getDate("start_date").toLocalDate());
                campaignFromDb.setEndDate(campaign.getDate("end_date").toLocalDate());
                campaignFromDb.setName(campaign.getString("name"));

                String selectStatus = "SELECT * FROM status WHERE id = " + campaign.getInt("status_id;");
                ResultSet resultSet = statement.executeQuery(selectStatus);
                Status status = new Status();
                while (resultSet.next()) {
                    status.setId(resultSet.getInt("id"));
                    status.setNumber(resultSet.getInt("number"));
                    status.setState(resultSet.getString("name"));
                }

                campaignFromDb.setStatus(status);
                campaignsFromDb.add(modelMapper.map(campaign, CampaignDto.class));
            }

            List<SummariesDto> summariesDtos = new ArrayList<>();
           campaignsFromDb.forEach(campaignDto -> {
               SummariesDto summariesDto = new SummariesDto();
               summariesDto.setId(campaignDto.getId());
               summariesDto.setStatus(campaignDto.getStatus());
               summariesDto.setCampaignsName(campaignDto.getName());
               summariesDto.setCountAds(adDao.getCountAdsByCampaignsId(campaignDto.getId()));
               summariesDtos.add(summariesDto);
           });
           campaign.close();
           return summariesDtos;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}
