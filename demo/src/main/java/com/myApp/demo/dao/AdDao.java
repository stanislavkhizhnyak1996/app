package com.myApp.demo.dao;

import com.myApp.demo.dto.AdDto;
import com.myApp.demo.dto.PlatformDto;
import com.myApp.demo.model.Ad;
import com.myApp.demo.model.Status;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.myApp.demo.Constants.DbConstant.PLANED_STATUS_ID;

@Repository
public class AdDao {
    private DataSource dataSource;
    private StatusDao statusDao;
    private PlatformDao platformDao;
    private ModelMapper modelMapper;

    @Autowired
    public AdDao(DataSource dataSource, StatusDao statusDao, PlatformDao platformDao, ModelMapper modelMapper) {
        this.dataSource = dataSource;
        this.statusDao = statusDao;
        this.platformDao = platformDao;
        this.modelMapper = modelMapper;
    }

    public List<AdDto> getAdsByCampaignsId(int id) {
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement()) {
            String getAdsByCampaignsId = "SELECT * FROM ads WHERE campaign_id = " + id + ";";
            ResultSet ads = statement.executeQuery(getAdsByCampaignsId);
            List<AdDto> adsFromDb = new ArrayList<>();

            while (ads.next()) {
                Ad ad = new Ad();
                ad.setId(ads.getInt("id"));
                ad.setName(ads.getString("name"));
                ad.setAssetUrl(ads.getString("assert_url"));
                ad.setPlatform(platformDao.getPlatformsForAdByAdId(ads.getInt("id")));

                String selectStatus = "SELECT * FROM status WHERE id = " + ads.getInt("status_id");
                ResultSet resultSet = con.createStatement().executeQuery(selectStatus);
                Status status = new Status();
                while (resultSet.next()) {
                    status.setId(resultSet.getInt("id"));
                    status.setNumber(resultSet.getInt("number"));
                    status.setState(resultSet.getString("name"));
                }

                ad.setStatus(status);
                adsFromDb.add(modelMapper.map(ad, AdDto.class));
            }
            return adsFromDb;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public int getCountAdsByCampaignsId(int id) {
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement()) {
            String getAdsByCampaignsId = "SELECT COUNT(*) AS total  FROM ads WHERE campaign_id = " + id + ";";
            ResultSet ads = statement.executeQuery(getAdsByCampaignsId);
            int count = 0;
            while (ads.next()) {
                count = ads.getInt("total");
            }
            return count;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    public AdDto getById(int id) {
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement()) {
            String select = "SELECT * FROM ads WHERE id = " + id;
            ResultSet resultSetAd = statement.executeQuery(select);
            Ad ad = new Ad();
            while (resultSetAd.next()) {
                ad.setId(resultSetAd.getInt("id"));
                ad.setAssetUrl(resultSetAd.getString("assert_url"));
                ad.setName(resultSetAd.getString("name"));

                String selectStatus = "SELECT * FROM status WHERE id = " + resultSetAd.getInt("status_id");
                ResultSet resultSet = con.createStatement().executeQuery(selectStatus);
                Status status = new Status();
                while (resultSet.next()) {
                    status.setId(resultSet.getInt("id"));
                    status.setNumber(resultSet.getInt("number"));
                    status.setState(resultSet.getString("name"));
                }

                ad.setStatus(status);
                ad.setPlatform(platformDao.getPlatformsForAdByAdId(id));
                ad.setCampaignsId(resultSetAd.getInt("campaign_id"));
            }
            resultSetAd.close();
            return modelMapper.map(ad, AdDto.class);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public AdDto add(AdDto ad) {
        try {
            try (Connection con = dataSource.getConnection();
                 Statement statement = con.createStatement()) {
                String createNewAd = "INSERT INTO ads (assert_url, name, campaign_id, status_id) VALUES(" + "'" + ad.getAssetUrl() + "'" + ", "
                    + "'" + ad.getName() + "'" + ", "
                    + ad.getCampaignsId() + ", "
                    + PLANED_STATUS_ID + ");";
                statement.execute(createNewAd, Statement.RETURN_GENERATED_KEYS);
                ResultSet rss = statement.getGeneratedKeys();
                int newAdId = 0;
                while (rss.next()) {
                    newAdId = rss.getInt("id");
                }
                List<PlatformDto> platforms = ad.getPlatform();
                if (!platforms.isEmpty()) {
                    for (PlatformDto platform : platforms) {
                        platformDao.addPlatformToAdByAdId(newAdId, platform.getId());
                    }
                    String addPlatformToAd = "UPDATE ads SET patforms = " + newAdId
                        + " WHERE id = " + newAdId + ";";
                    statement.execute(addPlatformToAd);
                }
                AdDto adDto = new AdDto();
                adDto = getById(newAdId);
                return adDto;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return ad;
        }
    }

    public AdDto update(AdDto ad) {
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement()) {
            platformDao.updatePlatformForAdByAdId(ad.getId(), ad.getPlatform());
            String sqlUpdate = "UPDATE ads SET assert_url = '" + ad.getAssetUrl() + "', "
                + "name = '" + ad.getName() + "',"
                + "status_id = " + ad.getStatus().getId() + ", "
                + "campaign_id = " + ad.getCampaignsId()
                + " WHERE id =" + ad.getId() + ";";
            statement.executeUpdate(sqlUpdate, Statement.RETURN_GENERATED_KEYS);
            int updatedAdId = 0;
            ResultSet generatedKeys = statement.getGeneratedKeys();
            while (generatedKeys.next()) {
                updatedAdId = generatedKeys.getInt(1);
            }
            return getById(updatedAdId);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public boolean deleteById(int id) {
        platformDao.deleteAdPlatformsByAdId(id);
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement()) {
            String deleteSql = "DELETE FROM ads WHERE id = " + id + ";";
            statement.execute(deleteSql);
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    public boolean deleteByCampaignId(int id) {
        platformDao.deleteAdPlatformsByAdId(id);
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement()) {
            String deleteSql = "DELETE FROM ads WHERE campaign_id = " + id + ";";
            statement.execute(deleteSql);
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }
}
