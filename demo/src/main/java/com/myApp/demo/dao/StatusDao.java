package com.myApp.demo.dao;

import com.myApp.demo.dto.StatusDto;
import com.myApp.demo.model.Status;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Repository
public class StatusDao {
    private DataSource dataSource;
    private ModelMapper modelMapper;

    @Autowired
    public StatusDao(DataSource dataSource, ModelMapper modelMapper) {
        this.dataSource = dataSource;
        this.modelMapper = modelMapper;
    }

    public StatusDto getById(int id) {
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement()
        ) {
            String select = "SELECT * FROM status WHERE id = " + id;
            ResultSet resultSet = statement.executeQuery(select);
            Status status = new Status();
            while (resultSet.next()) {
                status.setId(resultSet.getInt("id"));
                status.setNumber(resultSet.getInt("number"));
                status.setState(resultSet.getString("name"));
            }
            resultSet.close();
            return modelMapper.map(status, StatusDto.class);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}
