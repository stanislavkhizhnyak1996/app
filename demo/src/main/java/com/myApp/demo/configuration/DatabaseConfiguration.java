package com.myApp.demo.configuration;

import org.h2.jdbcx.JdbcDataSource;

import javax.sql.DataSource;

//@Configuration
public class DatabaseConfiguration {

    //@Bean
    public DataSource getDb() {
        JdbcDataSource ds = new JdbcDataSource();
        ds.setURL("jdbc:h2:mem:testdb");
        ds.setUser("sa");
        ds.setPassword("");
        return ds;
    }
}
