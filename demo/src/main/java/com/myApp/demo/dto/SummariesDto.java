package com.myApp.demo.dto;

import com.myApp.demo.model.Status;
import lombok.Data;

@Data
public class SummariesDto {
    private long id;
    private String campaignsName;
    private Status status;
    private int countAds;
}
