package com.myApp.demo.dto;

import lombok.Data;

import java.util.List;
@Data
public class AdDto {
    private int id;
    private String name;
    private StatusDto status;
    private List<PlatformDto> platform;
    private String assetUrl;
    private int campaignsId;
}
