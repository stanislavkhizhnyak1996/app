package com.myApp.demo.dto;

import lombok.Data;

@Data
public class SummariesFilter {
    private Filter filter;
    private Sort sort;

}
