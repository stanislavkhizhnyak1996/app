package com.myApp.demo.dto;

import com.myApp.demo.model.Status;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;
@Data
public class CampaignDto {
    private int id;
    private String name;
    private Status status;
    private LocalDate startDate;
    private LocalDate endDate;
    private List<AdDto> ads;
}
