package com.myApp.demo.dto;

import lombok.Data;

@Data
public class Filter {
    String name;
    String param;
}
