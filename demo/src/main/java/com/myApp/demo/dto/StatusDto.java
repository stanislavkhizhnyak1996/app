package com.myApp.demo.dto;

import lombok.Data;

@Data
public class StatusDto{
    private int id;
    private int number;
    private String state;
}
