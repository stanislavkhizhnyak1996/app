package com.myApp.demo.dto;

import lombok.Data;

@Data
public class PlatformDto {
    private int id;
    private int number;
    private String platform;
}
