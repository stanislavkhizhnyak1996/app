package com.myApp.demo.dto;

import lombok.Data;

@Data
public class Sort {
    String name;
}
