package com.myApp.demo.controller;

import com.myApp.demo.dto.CampaignDto;
import com.myApp.demo.service.CampaignsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/campaign")
public class CampaignsController {
    private final CampaignsService campaignsService;

    @Autowired
    public CampaignsController(CampaignsService campaignsService) {
        this.campaignsService = campaignsService;
    }

    @GetMapping(value = "getAd/{id}")
    @ResponseBody
    public CampaignDto getAdById(@PathVariable("id") int id) {
        return campaignsService.getById(id);
    }

    @PutMapping("/add")
    @ResponseBody
    public CampaignDto putAd(@RequestBody CampaignDto campaign) {
        return campaignsService.add(campaign);
    }

    @PostMapping("/update")
    @ResponseBody
    public CampaignDto updateAd(@RequestBody CampaignDto campaign) {
        return campaignsService.update(campaign);
    }

    @DeleteMapping("/delete")
    @ResponseBody()
    public HttpStatus delete(int id) {
        return campaignsService.delete(id) ? HttpStatus.OK : HttpStatus.NOT_FOUND;
    }
}
