package com.myApp.demo.controller;

import com.myApp.demo.dto.AdDto;
import com.myApp.demo.service.AdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ad")
public class AdController {
    private final AdService adService;

    @Autowired
    public AdController(AdService adService) {
        this.adService = adService;
    }

    @GetMapping(value = "/getAd/{id}")
    @ResponseBody
    public AdDto getAdById(@PathVariable("id") int id) {
        return adService.getById(id);
    }

    @PutMapping("/add")
    @ResponseBody
    public AdDto addAd(@RequestBody AdDto ad) {
        return adService.add(ad);
    }

    @PostMapping("/update")
    @ResponseBody
    public AdDto updateAd(@RequestBody AdDto ad) {
        return adService.update(ad);
    }

    @DeleteMapping("/delete")
    @ResponseBody()
    public HttpStatus delete(int id) {
        return adService.deleteById(id) ? HttpStatus.OK : HttpStatus.NOT_FOUND;
    }
}
