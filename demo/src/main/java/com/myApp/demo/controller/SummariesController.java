package com.myApp.demo.controller;

import com.myApp.demo.dto.SummariesDto;
import com.myApp.demo.dto.SummariesFilter;
import com.myApp.demo.service.SummariesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/summaries")
public class SummariesController {
    private final SummariesService summariesService;

    @Autowired
    public SummariesController(SummariesService summariesService) {
        this.summariesService = summariesService;
    }

    @GetMapping(value = "getAll")
    @ResponseBody
    public List<SummariesDto> getSummaries(SummariesFilter filter) {
        return summariesService.getSummariesByFilter(filter);
    }
}
