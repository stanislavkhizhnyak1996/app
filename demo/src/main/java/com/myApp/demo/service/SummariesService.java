package com.myApp.demo.service;

import com.myApp.demo.dao.SummariesDao;
import com.myApp.demo.dto.SummariesDto;
import com.myApp.demo.dto.SummariesFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

import static java.util.Objects.nonNull;

@Service
public class SummariesService {
    private final SummariesDao summariesDao;

    @Autowired
    public SummariesService(SummariesDao summariesDao) {
        this.summariesDao = summariesDao;
    }

    public List<SummariesDto> getSummariesByFilter(SummariesFilter summariesFilter) {
        List<SummariesDto> summariesDtos;
        if (nonNull(summariesFilter.getFilter())) {
            summariesDtos = summariesDao.getByParam(summariesFilter.getFilter());
        } else {
            summariesDtos = summariesDao.getAll();
        }

        String sortPropertyName = summariesFilter.getSort() != null && !summariesFilter.getSort().getName().isEmpty() ?
            summariesFilter.getSort().getName() : "";

        switch (sortPropertyName) {
            case "name": {
                summariesDtos.sort(Comparator.comparing(SummariesDto::getCampaignsName));
                break;
            }
            case "status": {
                summariesDtos.sort(Comparator.comparingInt((SummariesDto o) -> o.getStatus().getNumber()));
                break;
            }
            case "count": {
                summariesDtos.sort(Comparator.comparingInt(SummariesDto::getCountAds));
                break;
            }
        }

        return summariesDtos;
    }
}
