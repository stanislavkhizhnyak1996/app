package com.myApp.demo.service;

import com.myApp.demo.dao.AdDao;
import com.myApp.demo.dto.AdDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdService {
    private final AdDao adDao;

    @Autowired
    public AdService(AdDao adDao) {
        this.adDao = adDao;
    }

    public AdDto getById(int id) {
        return adDao.getById(id);
    }

    public AdDto add(AdDto ad) {
        return adDao.add(ad);
    }

    public AdDto update(AdDto ad) {
        return adDao.update(ad);
    }

    public boolean deleteById(int id) {
        return adDao.deleteById(id);
    }

}
