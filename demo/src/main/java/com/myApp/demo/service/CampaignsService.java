package com.myApp.demo.service;

import com.myApp.demo.dao.CampaingDao;
import com.myApp.demo.dto.CampaignDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CampaignsService {
    private final CampaingDao campaingDao;

    @Autowired
    public CampaignsService(CampaingDao campaingDao) {
        this.campaingDao = campaingDao;
    }

    public CampaignDto getById(int id) {
        return campaingDao.getById(id);
    }

    public CampaignDto add(CampaignDto campaign) {
        return campaingDao.add(campaign);
    }

    public CampaignDto update(CampaignDto campaign) {
        return campaingDao.update(campaign);
    }

    public boolean delete(int id) {
        return campaingDao.delete(id);
    }
}
