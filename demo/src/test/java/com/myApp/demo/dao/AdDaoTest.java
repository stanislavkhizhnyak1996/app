package com.myApp.demo.dao;

import com.myApp.demo.dto.*;
import com.myApp.demo.service.SummariesService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Collections;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AdDaoTest {

    @Autowired
    private AdDao adDao;
    @Autowired
    private CampaingDao campaingDao;
    @Autowired
    private SummariesService summariesService;

    @Test
    public void testAddedAllModelInDb() {
        AdDto ad = new AdDto();
        ad.setName("add");
        PlatformDto platform = new PlatformDto();
        platform.setPlatform("WEB");
        platform.setId(1);
        platform.setNumber(0);
        ad.setPlatform(Collections.singletonList(platform));
        ad.setAssetUrl("www.url.com");
        ad.setCampaignsId(1);

        CampaignDto campaign = new CampaignDto();
        campaign.setAds(Collections.singletonList(ad));
        campaign.setName("golden");
        campaign.setStartDate(LocalDate.now());
        campaign.setEndDate(LocalDate.now());
        CampaignDto campaign1 = campaingDao.add(campaign);
        System.out.println(campaign1);
        System.out.println(adDao.getAdsByCampaignsId(1));
        AdDto ad2 = new AdDto();
        ad2.setId(1);
        ad2.setName("add");
        PlatformDto platform2 = new PlatformDto();
        platform2.setPlatform("web");
        platform2.setId(1);
        platform2.setNumber(0);
        ad2.setPlatform(Collections.singletonList(platform2));
        ad2.setAssetUrl("www.url-nigas.com");
        ad2.setCampaignsId(1);
        StatusDto status = new StatusDto();
        status.setId(2);
        ad2.setStatus(status);
        System.out.println(adDao.update(ad2));
        System.out.println(adDao.getAdsByCampaignsId(1));
        campaign1.setName("Чернослив");
        System.out.println(campaingDao.update(campaign1));
        SummariesFilter filter = new SummariesFilter();
        filter.setFilter(new Filter());
        filter.setSort(new Sort());
        System.out.println(summariesService.getSummariesByFilter(new SummariesFilter()));
        Assert.assertTrue(campaingDao.delete(campaign1.getId()));
        System.out.println(campaingDao.getById(campaign1.getId()));
        System.out.println(adDao.getAdsByCampaignsId(campaign1.getId()));
    }
}
